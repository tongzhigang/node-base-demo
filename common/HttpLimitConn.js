const schedule = require('node-schedule');

class HttpLimitConn {
	constructor({ limit, space, whitelist, debug }) {
		this.limit = limit || 60;
		this.space = space || '30 * * * * *';
		this.whitelist = whitelist;
		this.debug = debug ? true : false;
		this.pools = {};
		this.startSchedule();
	}
	getClientIp(req) {
		return (
			req.headers['x-forwarded-for'] ||
			req.connection.remoteAddress ||
			req.socket.remoteAddress ||
			req.connection.socket.remoteAddress ||
			''
		);
	}
	verifyClient(req) {
		if (this.debug) return true;
		let ip = this.getClientIp(req);
		if (!(ip in this.pools)) {
			this.pools[ip] = 0;
		}
		if (this.pools[ip]++ > this.limit) {
			return false;
		} else {
			return true;
		}
	}
	startSchedule() {
		schedule.scheduleJob(this.space, () => {
			for (var key in this.pools) {
				delete this.pools[key];
			}
		});
	}
	verifyOrigin(req) {
		if (process.env.NODE_ENV === 'development' || this.pools.length === 0) {
			return true;
		} else {
			let origin = req.headers['origin'];
			if (origin) {
				return this.whitelist.indexOf(origin.toLocaleLowerCase()) > -1;
			} else {
				return true;
			}
		}
	}
}

module.exports = HttpLimitConn;
