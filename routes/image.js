const express = require('express');
const router = express.Router();
const Mock = require('mockjs');
const Random = Mock.Random;
const MsgJsonHelper = require('../common/MsgJsonHelper');

router.get('/random', (req, res) => {
    let w = req.query.w || '200';
    let h = req.query.h || '200';
    let bg = req.query.bg || "f2f2f2"
    let text = req.query.text || w + "x" + h;
    res.json(MsgJsonHelper.DefaultJson(Random.image(w + 'x' + h, '#' + bg, text), true, ''));
});

module.exports = router;